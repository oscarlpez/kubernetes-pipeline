# Kubernetes cluster
Instalacion de un cluster kubernetes desde cero usando Ansible y kubeadm

* Clonamos el repositorio
* Provison de las [SSH KEYS]
en los nodos a desplegar, esto es un requerimiento para los nodos gestionados por  [Ansible]
* Este despliegue está probado para Ubuntu 20.04

## Instalación

1- Actualizar fichero ``hosts`` de inventario sustituyendo las IPs para el cluster 
* IP control plane 
* IP worker node
```
[control_plane]
control-plane ansible_host=IP_control_plane ansible_user=root

[workers]
worker-0 ansible_host=IP_worker_node ansible_user=root

[all:vars]
ansible_python_interpreter=/usr/bin/python3
```

2- Comprobar el fichero de inventario y que alacanzamos las IPs del inventario
```
$ ansible-inventory -i hosts –-list
```
```
$ ansible control-plane -i hosts -m ping
```
```
$ ansible workers -i hosts -m ping
```


3- Modificar fichero ``control-plane.yml`` para indicar la ejecución en la IP configurada en el fichero de inventario

```
- hosts: control_plane 
```

4- Ejecutar ``main.yml``
```
$ ansible-playbook -i hosts main.yml
```

5- Comprobar el despliegue
* Control plane
```
$ ssh root@<ip_control_plane>
```
```
# su ubuntu
$ kubectl get nodes
```
Mostrando la siguiente salida que indica que tanto el control plane como el worker node están listos
```
NAME                STATUS     ROLES                  AGE     VERSION
control-plane       Ready      control-plane,master   2m36s   v1.22.4
worker-0            Ready      <none>                 61s     v1.22.4
```

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
   [SSH Keys]: <https://docs.digitalocean.com/products/droplets/how-to/add-ssh-keys/>
   [Ansible]: <https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#managed-node-requirements>


