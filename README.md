# Kubernetes CI/CD pipeline

El objetivo es crear un cluster de kubernetes desde cero desplegado en un proveedor de servicios de cloud y utilizando las herramientas necesarias que permitan automatizar el despliegue de manera segura.

CI/CD Pipeline for deploying a kubernetes cluster using Terraform

Infraestructure as Code - IaC - with Terraform

## Infraestructures as Code - IaC - with Terraform and Ansible

Usaremos las siguientes tools

* GitLab
* Jenkins
* Terraform
* Ansible
* Digital Ocean


## Steps

* Basic Deployment (Terraform + Ansible)
* CI GitLab + Jenkins
* CD Deployment with Terraform + Ansible - GitOps

## Basic Deployment 

Previo a llevar a cabo una integración en el pipeline CI/CD con Jenkins, hemos llevado a cabo la implementación de manera desacoplada de de una IaC mediante Terraform y Ansible para desplegar un cluster de Kubernetes.

La implementación descrita a continuación ha sido necesaria para depurar errores y ajustar y probar la configuración de Ansible que luego integraremos con GitLab y Jenkins en un ciclo DevOps para GitOps.

Utilizaremos Digital Ocean como proveedor de servicios y previamente necesitamos configurar 

* Personal Access Token
* SSH keys
* S3 backend storage

## Configuración


### Digital Ocean 

* Personal Access Token
* SSH keys
* S3 backend storage

S3 Simple Storage Service. Usaremos como un servicio de backend para guardar la configuración de estado de Terraform.

* Crear S3 storage
* Generar Access Keys/Secret Keys


Para gestionar el acceso a los Spaces (almacenamiento en Digital Ocean) hay que generar una access key y una secret key


### Terraform
Fichero de estado ``terraform state``

Es importante conservar el fichero de estado Terraform para sucesivas ejecuciones 
 y es importante mantener este fichero de manera centralizada, esto se facilita mediante la ubicación del fichero de estado en el provvedor de servicios de Cloud, de manera que podemos conservar el estado del despliegue de Terrafrm en sucesivas ejecuciones.

Una vez que el fichero terraform ``tfstate`` está centralizado podemos ejecutar Terraform referenciando a este fichero desde varios entornos, manteniendo este estado y modificando el mismo desde por ejemplo la integración en Jenkins y GitLab.



Fichero principal Terrafom  ``main.tf``
- Inicialmente lo guardamos el fichero de estado localmente

```
$ terraform init
````
- Después y una vez configurado el backend, es necesario incluir la configuración del backend en el ``main.tf``  

- Hay que dar el nombre al fichero de estado Terraform creado en Spaces/Digital Ocean ``idi-terraform.tfstate`` que asignaremos en el campo ``key``


```
terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }

    backend "s3" {
        endpoint                    = "https://fra1.digitaloceanspaces.com/"
        key                         = "idi-terraform.tfstate"
        bucket                      = "idi-devops"
        region                      = "us-east-1"   
        skip_credentials_validation = true
        skip_metadata_api_check     = true
    }
}

variable "do_token" {}

provider "digitalocean" {
  token   = var.do_token
}

resource "digitalocean_droplet" "kubernetes-control" {
  image    = "ubuntu-20-04-x64"
  region   = "nyc1"
  count    = 1
  ssh_keys = ["d4:ec:01:2f:c5:ee:58:ed:ac:10:d5:f8:d8:78:31:ba","d2:75:92:fd:da:2e:1e:01:f6:a3:a7:34:30:25:fc:59"]
  size     = "s-2vcpu-2gb"
  name     = "control-plane"
  tags     = ["kube-control"]
}

```

* Finalmente volvemos a inicializar Terraform para guardar la configuración en el proveedor de servicios cloud (Digital Ocean, AWS, u otros). Hay que pasar el Access Key y el Secret Key para acceso al S3 Simple Storage Service. Tenemos varias maneras de hacerlo en este caso utilizaremos variables que unicamente están en uso en la shell de trabajo

## Terrafom init
Una vez que disponemos de los datos necesarios

Inicializamos Terraform

```
$ terraform init --backend-config="access_key=$ACCESS_KEY" --backend-config="secret_key=$SECRET_KEY"
```

Esto generara el archivo de estado en el backend, lo que nos permitira mantenerlo centralizado y ejecutar Terraform tanto desde el despliegue basico como desde el pipeline CI/CD

Tras lanzar el comando en terraform, el fichero de estado se ha montado en el almacenamiento S3 en el proveedor de servicios de cloud - Digital Ocean.

Con lo cual cada vez que se ejecute Terraform en 1) local o en 2) desde el pipeline GitLab/.Jenkins el fichero creado esta compartido en una ubicación centralizada en el proveedor de servicio de cloud.

``main.tf``  de esta forma no esta ligado al pipeline ni a los datos locales, si no que  estará en un sitio remoto centralizada. Es una de las maneras de centralizar la configuración del backend. 


* Hay otras opciones, en este caso hemos utilizado el backend S3 del proveedor Digital Ocean.

* Terraform Cloud. Available backends
https://www.terraform.io/language/settings/backends/s3



Visualizamos el plan que se genera en funcion a la definicion de nuestro fichero ``main.tf``
```
$ terraform plan -var do_token=$ACCESS_TOKEN
```
Aplicamos el plan con ``terraform apply`` 
```
$ terraform apply -auto-approve -var do_token=$ACCESS_TOKEN -var ssh_key=.id_rsa
```

## Configuración Kubernetes

Tras el despliegue anterior tenemos configurado tanto el control plane como el worker node y solo hay que añadir el worker al cluster

* Control Plane
```
$ ssh root@control_plane_ip
# su ubuntu
```

Comprobamos la instalación
```
$ kubectl get nodes
```
Y la salida nos devuelve 
```
NAME       STATUS     ROLES                  AGE     VERSION
control    Ready      control-plane,master   2m36s   v1.22.4
```
Y despues ejecutamos el comando que nos va a permitir hacer el join en el worker

```
$ kubeadm token create --print-join-command
```
Con la salida de este comando nos logeamos en el Worker node para llevar a cabo la inicialización

* Worker
```
$ ssh root@worker_ip
```
Y ejecutamos la salida al comando anterior

```
# kubeadm join kubernetes_control_plane_ip:6443 --token id_token --discovery-token-ca-cert-hash
sha256:sha256_id 
```

Comprobamos el estado del cluster desde el control plane
```
$ kubectl get nodes
```
Obteniendo en este caso

```
NAME       STATUS     ROLES                  AGE     VERSION
control    Ready      control-plane,master   2m36s   v1.22.4
worker01   Ready      <none>                 61s     v1.22.4
```

Tras esta comprobación podemos lanzar un despliegue de un deployment y un service para probar el funcionamiento del cluster

Desde el control plane ejecutamos

```
$ kubectl create deployment nginx --image=nginx
$ kubectl expose deploy nginx --port 80 --target-port 80 --type NodePort
```

```
$ kubectl get services
```

Y probamos el servicio desplegado como NodePort en la IP del Nodo Worker




## Destrucción del entorno

* Finalmente se destruye el deployment mediante

```
terraform destroy -auto-approve -var do_token=$ACCESS_TOKEN -var ssh_key=.id_rsa
```


## Notas Terraform

- Si no modificamos el ``main.tf`` no se llevan a cabo cambios tras ejecutar  ``terraform plan``
- Las modificaciones en el ``main.tf `` sobre un recurso ya existente recurso para incluir una nueva acción sobre el provisioner en dicho recurso  ``provisioner`` no provocan cambios tras ejecutar ``terraform plan``


# Automatizar IaC mediante GitOps

Utilizaremos la integracion Jenkins GitLab para la creacion y destruccion del entorno configurando un pipeline

* Integración en un ciclo de operaciones mediante Git (GitOps)
* Configuracion integracion GitLab Jenkins

# Jenkins

Jenkins, nos va a permitir automatizar la integración continua (CI) y en Jenkins organizaremos una serie de acciones o ``stages`` que nos van a facilitar la implementación automatizada del proceso de integración continua.

Configuración básica:

Esta configuración del fichero ``Jenkins file`` nos permitirá probar que Jenkins puede hacer un build del proyecto en GitLab tras configurar la integración.

<pre><code>
pipeline {
    agent any
    stages {
        stage('hello') {
            steps {
                sh 'Hello World'
            }
        }
    }
}
</pre></code>

Para el caso del despliegue de a IaC con Terraform y Ansible usaremos los siguientes stages en el pipeline que están ligadas a las fases de Terraform


* Init
* Apply
* Destroy

El fichero Jenkinsfile es el que se encuentra en el repositorio y a continuacion

<pre><code>
pipeline {
    agent any

    environment {
        ACCESS_TOKEN    = credentials('access_token')
        ACCESS_KEY      = credentials('access_key')
        SECRET_KEY      = credentials('secret_key')
        SSH_KEY         = credentials('ssh_key_file')
    }

    stages {

        stage('Init') {
            agent {
                docker {
                    image 'tuxotron/terransible'
                    args '--entrypoint=""'
                    reuseNode true
                }
            }
            steps {
                sh 'terraform init -backend-config="access_key=$ACCESS_KEY" -backend-config="secret_key=$SECRET_KEY"'
            }
        }
        stage('Apply') {
            when {
                branch 'main'
            }

            agent {
                docker {
                    image 'tuxotron/terransible'
                    args '-u root --entrypoint=""'
                    reuseNode true
                }
            }
            steps {
                sh 'terraform apply -auto-approve -var "do_token=$ACCESS_TOKEN" -var "ssh_key=$SSH_KEY"'
            }
        }
        stage('Destroy') {
            when {
                branch 'destroy'
            }

            agent {
                docker {
                    image 'tuxotron/terransible'
                    args '--entrypoint=""'
                    reuseNode true
                }
            }
            steps {
                sh 'terraform destroy -auto-approve -var "do_token=$ACCESS_TOKEN" -var "ssh_key=$SSH_KEY"'
            }
         }

     }
}
</pre></code>