terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }

}

variable "do_token" {}
variable "ssh_key" {}

provider "digitalocean" {
  token   = var.do_token
}

resource "digitalocean_droplet" "kubernetes-control" {
  image    = "ubuntu-20-04-x64"
  region   = "nyc1"
  count    = 1
  ssh_keys = ["d4:ec:01:2f:c5:ee:58:ed:ac:10:d5:f8:d8:78:31:ba","d2:75:92:fd:da:2e:1e:01:f6:a3:a7:34:30:25:fc:59"]
  size     = "s-2vcpu-2gb"
  name     = "control-plane"
  tags     = ["kube-control","ready","done"]

provisioner "remote-exec" {
    inline = ["sudo apt update", "sudo apt install python3 -y", "echo Done!"]

    connection {
      host        = self.ipv4_address
      type        = "ssh"
      user        = "root"
      private_key = file(var.ssh_key)
    }
  }
provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key=${var.ssh_key} ../kube-cluster/initial.yml"
  }

provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key=${var.ssh_key} ../kube-cluster/kube-dependencies.yml"
  }

provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key=${var.ssh_key} ../kube-cluster/control-plane.yml"
 }

}


resource "digitalocean_droplet" "kubernetes-worker" {
  image    = "ubuntu-20-04-x64"
  region   = "nyc1"
  count    = 1
  ssh_keys = ["d4:ec:01:2f:c5:ee:58:ed:ac:10:d5:f8:d8:78:31:ba","d2:75:92:fd:da:2e:1e:01:f6:a3:a7:34:30:25:fc:59"]
  size     = "s-2vcpu-2gb"
  name     = "worker-${count.index}"
  tags     = ["kube-worker","done"]


provisioner "remote-exec" {
    inline = ["sudo apt update", "sudo apt install python3 -y", "echo Done!"]

    connection {
      host        = self.ipv4_address
      type        = "ssh"
      user        = "root"
      private_key = file(var.ssh_key)
    }
  }

provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key=${var.ssh_key} ../kube-cluster/initial.yml"
  }

provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u root -i '${self.ipv4_address},' --private-key=${var.ssh_key} ../kube-cluster/kube-dependencies.yml"
  }

}


output "droplet_ip_addresses_control" {
  value = {
    for droplet in digitalocean_droplet.kubernetes-control:
    droplet.name => droplet.ipv4_address
 }

}
output "droplet_ip_addresses_workers" {
  value = {
    for droplet in digitalocean_droplet.kubernetes-worker:
    droplet.name => droplet.ipv4_address
 }

}


